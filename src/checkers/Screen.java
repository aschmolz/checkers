package checkers;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Screen extends JPanel {

	private Game currentGame;
	private Dimension res;
	private int squareWidth;
	private int squareHeight;
	private Point boardSquares = new Point(8, 8);
	private Square[][] board;
	private Square mouseSquare = null;

	public Screen(Game game) {
		this.init();
		System.out.println(this.getSize());
		this.res = this.getSize();
		this.squareWidth  = this.res.width/this.boardSquares.x;
		this.squareHeight = this.res.height/this.boardSquares.y;
		this.currentGame = game;

		this.board = this.currentGame.getBoard();
	}

	public void init(){
		this.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				Screen.this.click();
			}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {}

			@Override
			public void mouseExited(MouseEvent e) {}
		});
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.squareWidth  = this.res.width/this.boardSquares.x;
		this.squareHeight = this.res.height/this.boardSquares.y;
		this.drawBackground(g);
		this.drawBoard(g);
		this.drawMouseSquare(g);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.setTitle(this.currentGame.getTurn() + "'s turn");
	}

	public void tick(){
		this.board = this.currentGame.getBoard();
		this.mouseSquare = this.getMouseSquare();
	}

	public void click(){
		if(this.mouseSquare == null)
			return;

		this.currentGame.click(this.mouseSquare);
	}

	public void drawBackground(Graphics g){
		Color background = new Color(80, 80, 80);
		g.setColor(background);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
	}

	public void drawPiece(Piece p, Graphics2D g2d){
		int x = p.getX() * this.squareWidth;
		int y = p.getY() * this.squareHeight;

		if(p.getType().equals("black"))
			g2d.setColor(Color.RED);
		else if(p.getType().equals("white"))
			g2d.setColor(Color.WHITE);
		else if(p.getType().equals("possible"))
			g2d.setColor(new Color(0, 255, 0, 100));

		int offset = 8; // even only

		g2d.fillOval(x + offset/2, y + offset/2, this.squareWidth - offset, this.squareHeight - offset);

		if(p.isKing()){ // draw crown
			int crownOffsetX = this.squareWidth/2;
			int crownOffsetY = this.squareHeight/2;
			g2d.setColor(Color.YELLOW);
			g2d.fillRect(x + crownOffsetX/2, y + crownOffsetY/2, this.squareWidth - crownOffsetX, this.squareHeight - crownOffsetY);
		}
	}

	public void drawBoard(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		int maxSize = 90;

		// Ensures the size of the squares can never exceed the max.
		if(this.squareWidth > maxSize) this.squareWidth = maxSize;
		if(this.squareHeight > maxSize) this.squareHeight = maxSize;

		this.res = this.getSize();

		for(int i = 0; i < this.boardSquares.y; i++){
			for (int j = 0; j < this.boardSquares.x; j++){
				Square currentSquare = this.board[i][j];
				g2d.setColor(currentSquare.getColor());

				int x = j * this.squareWidth;
				int y = i * this.squareHeight;

				g2d.fillRect(x, y, this.squareWidth, this.squareHeight); // draw squares

				// draw the coordinates
				g2d.setColor(Color.GREEN);
				g.drawString(currentSquare.x + ", " + currentSquare.y, x, y + this.squareHeight);

				if(currentSquare.getPiece() != null)
					this.drawPiece(currentSquare.getPiece(), g2d);
			}
		}
	}

	public Square getMouseSquare(){
		Point coor = this.getMousePosition();

		// check if the mouse is outside the bounds of the board
		if(coor == null)
			return null;

		if(coor.x >= this.squareWidth * this.boardSquares.x || coor.y >= this.squareHeight * this.boardSquares.y)
			return null;

		Square boardCoor = this.board[coor.y / this.squareHeight][coor.x / this.squareWidth];

		return boardCoor;
	}

	public void drawMouseSquare(Graphics g){
		if(this.mouseSquare == null)
			return;

		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g.setColor(new Color(255, 255, 0, 100));
		g.fillRect(this.mouseSquare.x * this.squareWidth, this.mouseSquare.y * this.squareHeight, this.squareWidth, this.squareHeight);

		Square currentSquare = this.board[this.mouseSquare.x][this.mouseSquare.y];
	}
}