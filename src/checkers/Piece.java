package checkers;

import java.util.ArrayList;

public abstract class Piece {
	protected boolean isKing = false;
	protected String type = "nocolor";
	protected String enemyColor = "nocolor";
	protected ArrayList<Possible> possibleMoves = new ArrayList<Possible>();
	protected int x;
	protected int y;
	protected Square[][] boardState;
	protected ArrayList<Possible> movesToCheck = new ArrayList<Possible>();
	private int size;

	public Piece(){

	}

	public boolean isKing(){
		return this.isKing;
	}

	public String getType(){
		return this.type;
	}

	public void king(){
		this.isKing = true;
	}

	private boolean withinBounds(int x, int y){
		return (x < this.size && x >= 0) && (y < this.size && y >= 0);
	}

	private void checkMove(int directionX, int directionY){

		int touching_x = this.x + (1 * directionX); // the x coordinate of the piece directly adjacent (diagonally) to the current piece in the 'directionX'
		int touching_y = this.y + (1 * directionY); // the y ...

		if(this.withinBounds(touching_x, touching_y)) { // check if the attempted move is within the bounds of the board
			Piece currentPiece = this.boardState[touching_y][touching_x].getPiece();
			if(currentPiece == null) { // check if there is no piece
				Possible p = new Possible(touching_x, touching_y);
				this.possibleMoves.add(p);
			}
		}
	}

	public void checkHop(Square currentSquare, int directionX, int directionY) {

		int touching_x = currentSquare.x + (1 * directionX); // the x coordinate of the piece directly adjacent (diagonally) to the current piece in the 'directionX'
		int touching_y = currentSquare.y + (1 * directionY); // the y ...

		int hop_x = currentSquare.x + (2 * directionX);
		int hop_y = currentSquare.y + (2 * directionY);

		if (this.withinBounds(hop_x, hop_y)) { // check if the attempted y move is within the bounds of the board
			Piece currentPiece = this.boardState[touching_y][touching_x].getPiece();
			if (currentPiece != null) { // if there is a piece touching
				Piece currentHop = this.boardState[hop_y][hop_x].getPiece();
				if (currentPiece.getType().equals(this.enemyColor) && currentHop == null) { // if the touching piece belongs to the enemy & the space after is empty
					Possible p = new Possible(this.boardState[hop_y][hop_x]);
					p.addEat(currentPiece); // add the piece hopped in this check to the list
					for (Piece eat : this.movesToCheck.get(0).getEating()) {
						p.addEat(eat); // add all the previous hopped pieces to the list
					}

					this.possibleMoves.add(p);
					this.movesToCheck.add(p);
					this.boardState[hop_y][hop_x].setPiece(p);
				}
			}
		}
	}

	public ArrayList<Possible> getPossibleMoves(Square[][] boardIn){
		this.boardState = boardIn;
		this.size = this.boardState.length;

		if(this.type.equals("black") || (this.type.equals("white") && this.isKing)){ // up
			this.checkMove(-1, -1); // left, up
			this.checkMove(1, -1);  // right, up
		}

		if(this.type.equals("white") || (this.type.equals("black") && this.isKing)){ // down
			this.checkMove(-1, 1); // left, down
			this.checkMove(1, 1);  // right, down
		}

		this.movesToCheck.add(new Possible(this.boardState[this.y][this.x]));
		Square currentSquare;

		while(this.movesToCheck.size() > 0) {
			currentSquare = this.movesToCheck.get(0).getSquare();

			if(this.type.equals("black") || (this.type.equals("white") && this.isKing)) { // up
				this.checkHop(currentSquare, -1, -1); // left, up
				this.checkHop(currentSquare, 1, -1);  // right, up
			}

			if(this.type.equals("white") || (this.type.equals("black") && this.isKing)) { // down
				this.checkHop(currentSquare, -1, 1); // left, down
				this.checkHop(currentSquare, 1, 1);  // right, down
			}

			this.movesToCheck.remove(0);
		}
		return this.possibleMoves;
	}

	public void setCoor(int x, int y){
		this.x = x;
		this.y = y;
	}

	public int getX(){
		return this.x;
	}

	public int getY(){
		return this.y;
	}
}