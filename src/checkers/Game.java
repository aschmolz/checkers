package checkers;

import java.awt.*;
import java.util.ArrayList;

public class Game {

	private Square[][] board = new Square[8][8];
	private boolean showingPossible = false;
	private String turn = "black";
	private Square lastClicked;
	private ArrayList<Possible> possible = new ArrayList<Possible>();
	private boolean isRunning;

	public Game(){
		this.isRunning = true;
		this.createBoard();
		this.addPieces();
	}

	public boolean isRunning(){
		return this.isRunning;
	}

	public void createBoard(){
		for(int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[i].length; j++) {
				if (i % 2 == 0) {
					if (j % 2 == 0){
						this.board[i][j] = new Square(Color.WHITE, j, i);
					} else {
						this.board[i][j] = new Square(Color.BLACK, j, i);
					}
				} else {
					if (j % 2 == 0) {
						this.board[i][j] = new Square(Color.BLACK, j, i);
					} else {
						this.board[i][j] = new Square(Color.WHITE, j, i);
					}
				}
			}
		}
	}

	public void addPieces() {
		for (int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[i].length; j++) {
				if(i < 3){ // white pieces
					if(i == 1){
						if(j % 2 == 0){
							this.board[i][j].setPiece(new WhitePiece());
						}
					} else {
						if(j % 2 != 0){
							this.board[i][j].setPiece(new WhitePiece());
						}
					}
				} else if(i > 4){
					if(i == 6){
						if(j % 2 != 0){
							this.board[i][j].setPiece(new BlackPiece());
						}
					} else {
						if(j % 2 == 0){
							this.board[i][j].setPiece(new BlackPiece());
						}
					}
				}
			}
		}
	}

	public void click(Square square){
		this.handleClick(square);
	}

	public void clearPossible(){

		this.possible.clear();

		for(int i = 0; i < this.board.length; i++) {
			for (int j = 0; j < this.board[i].length; j++) {
				Square currentSquare = this.board[i][j];
				if(currentSquare.getPiece() != null){
					if(currentSquare.getPiece().getType().equals("possible")){
						currentSquare.removePiece();
					}
				}
			}
		}
	}

	public String getTurn(){
		return this.turn;
	}

	private boolean checkKing(Piece p){
		return (p.getType().equals("black") && p.y == 0) || (p.getType().equals("white") && p.y == 7);
	}

	public void handleClick(Square square){
		Square currentSquare = this.board[square.y][square.x];
		Piece currentPiece = currentSquare.getPiece();
		if(currentPiece == null) // if there is no piece do nothing
			return;

		// if the piece clicked on was a possible move, move the last piece to the new square and remove the possible move markers
		if(currentPiece.getType().equals("possible")){

			currentSquare.setPiece(this.lastClicked.getPiece());
			this.board[this.lastClicked.y][this.lastClicked.x].removePiece();
			
			if(this.checkKing(currentSquare.getPiece())) {
				currentSquare.getPiece().king();
			}

			for(Possible p : this.possible){ // find which possible move the user selected
				if(p.getCoor().equals(new Point(square.x, square.y))){
					for(Piece eat : p.getEating()){
						this.board[eat.getY()][eat.getX()].removePiece(); // remove all the enemy pieces hopped by this move
					}
				}
			}
			this.clearPossible();

			// check for winner by pieces remaining

			boolean blackWin = true;
			boolean whiteWin = true;

			for (Square[] a : this.board) {
				for (Square s : a) {
					if(s.getPiece() == null)
						continue;

					if(s.getPiece().getType().equals("black")){
						whiteWin = false;
					} else if(s.getPiece().getType().equals("white")){
						blackWin = false;
					}
				}
			}

			if(blackWin){
				System.out.println("Black wins!");
				this.isRunning = false;
				return;
			}
			if(whiteWin){
				System.out.println("White wins!");
				this.isRunning = false;
				return;
			}

			// check for winner by legal moves



			// switch turn
			if(this.turn.equals("black"))
				this.turn = "white";
			else
				this.turn = "black";

			return;
		}

		this.lastClicked = square;

		if(this.showingPossible){
			this.clearPossible();
			this.showingPossible = false;
		}

		if(this.turn.equals(square.getPiece().getType())){
			this.possible = square.getPiece().getPossibleMoves(this.board);
		}

		for(Possible p : this.possible){ // add the "possible" pieces to the board
			this.board[p.getY()][p.getX()].setPiece(p);
		}

		this.showingPossible = true;
	}

	public Square[][] getBoard(){
		return this.board;
	}

	public void tick(){

	}
}