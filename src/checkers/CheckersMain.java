package checkers;

import javax.swing.*;
import java.awt.*;

public class CheckersMain {
	public static void main(String[] args) {
		JFrame window = new JFrame();
		window.setLayout(new BorderLayout());
		window.setSize(1024, 768);

		Game game = new Game();

		Screen s = new Screen(game);
		window.add(s);

		window.setVisible(true);

		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while(game.isRunning()){
			s.tick();
			window.repaint();
			try{
				Thread.sleep(20);
			} catch(java.lang.InterruptedException ex){
				ex.printStackTrace();
			}

		}
		System.out.println("game over");
	}
}