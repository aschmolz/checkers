package checkers;
import java.awt.*;
import java.util.ArrayList;

public class Square {
	private Color color;
	public int x;
	public int y;
	private Piece piece = null;

	public Square(Color color, int x, int y){
		this.color = color;
		this.x = x;
		this.y = y;
	}

	public Color getColor(){
		return this.color;
	}

	public Piece getPiece() {
		return this.piece;
	}

	public void setPiece(Piece piece){
		piece.setCoor(this.x, this.y);
		this.piece = piece;
	}

	public void removePiece(){
		this.piece = null;
	}

}