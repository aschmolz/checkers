package checkers;

import java.util.ArrayList;

/**
 * Created by Austin on 1/1/2016.
 */
public class BlackPiece extends Piece{
    public BlackPiece(){
        this.type = "black";
        this.enemyColor = "white";
        this.isKing = false;
    }
}
