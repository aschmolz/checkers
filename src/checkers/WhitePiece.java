package checkers;

import java.util.ArrayList;

/**
 * Created by Austin on 1/1/2016.
 */
public class WhitePiece extends Piece{
    public WhitePiece(){
        this.type = "white";
        this.enemyColor = "black";
        this.isKing = false;
    }

}
