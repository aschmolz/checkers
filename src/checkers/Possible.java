package checkers;

import java.awt.*;
import java.util.ArrayList;

public class Possible extends Piece{
    private int x;
    private int y;
    private ArrayList<Piece> eating = new ArrayList<Piece>();
    private Square s;

    public Possible(int x, int y){
        this.x = x;
        this.y = y;
        this.type = "possible";
    }

    public Possible(Square s){
        this.s = s;
        this.x = s.x;
        this.y = s.y;
        this.type = "possible";
    }

    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public Point getCoor(){
        return new Point(this.x, this.y);
    }

    public void addEat(Piece piece){
        this.eating.add(piece);
    }

    public Square getSquare(){
        return this.s;
    }

    public ArrayList<Piece> getEating(){
        return this.eating;
    }

    @Override
    public ArrayList<Possible> getPossibleMoves(Square[][] board) {
        return null;
    }
}
